package ru.emdev.user.ddm.field;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Organization;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.ServletResponseUtil;
import com.liferay.portal.kernel.struts.BaseStrutsAction;
import com.liferay.portal.kernel.struts.StrutsAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.PortletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

@Component(immediate = true, property = "path=/portal/userddm/users", service = StrutsAction.class)
public class UserDDMFormFieldStrutsAction extends BaseStrutsAction {

    @Reference
    private UserLocalService userLS;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

        try {
            try {
                ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
                String searchQuery = ParamUtil.get(request, "inputVal", StringPool.BLANK).toLowerCase();
                List<User> findedUsers = userLS.search(themeDisplay.getCompanyId(), searchQuery,
                        WorkflowConstants.STATUS_APPROVED, null,
                        0, 10, (OrderByComparator) null);
                JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
                for (User user : findedUsers) {
                    JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
                    String name = user.getFullName().toLowerCase();
                    String screenName = user.getScreenName().toLowerCase();
                    String emailAddress = user.getEmailAddress().toLowerCase();
                    String jobTitle = user.getJobTitle().toLowerCase();
                    String organizations = "";
                    if (user.hasOrganization()) {
                        organizations = user.getOrganizations().stream().map(Organization::getName)
                                .collect(Collectors.joining(", "));
                    }
//                    System.out.println(String.format("name - %s | sname -%s | email - %s | job - %s | q = %s", name,
//                            screenName, emailAddress, jobTitle, searchQuery));
                    if (((name.contains(searchQuery))
                            || (screenName.contains(searchQuery))
                            || (emailAddress.contains(searchQuery))
                            || (jobTitle.contains(searchQuery)))) {
                        jsonObject.put("fullname", user.getFullName());
                        jsonObject.put("displayname", user.getScreenName());
                        jsonObject.put("email", user.getEmailAddress());
                        jsonObject.put("jobTitle", user.getJobTitle());
                        jsonObject.put("id", user.getUserId());
                        jsonObject.put("organizations", organizations);
                        jsonArray.put(jsonObject);
                    }
                }
                response.setContentType(ContentTypes.APPLICATION_JSON);
                response.setCharacterEncoding("UTF-8");
                response.setStatus(HttpServletResponse.SC_OK);
                ServletResponseUtil.write(response, jsonArray.toString());
            } catch (Exception e) {
                JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
                jsonObject.putException(e);
                ServletResponseUtil.write(response, jsonObject.toString());
            }
        } catch (Exception e) {
            throw new PortletException(e);
        }
        return null;
    }
}
