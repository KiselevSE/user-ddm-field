<#include "../init-ext.ftl">

<#assign multiple = false>
<#if fieldStructure.multiItem?? && (escape(fieldStructure.multiItem) == "true")>
	<#assign multiple = true>
</#if>

<#assign usercardUrl = fieldStructure.usercardUrl!"/web/guest/home/-/usercards/">
<#assign emailOption = fieldStructure.emailOption!"nowhere">
<#assign jobTitleOption = fieldStructure.jobTitleOption!"nowhere">
<#assign organizationsOption = fieldStructure.organizationsOption!"nowhere">


<@liferay_aui["field-wrapper"] data=data helpMessage=escape(fieldStructure.tip)>
	<#if fieldValue = "">
		<#assign values = []>
	<#else>
		<#assign values = fieldValue?split(",", "r")>
	</#if>

	<@liferay_aui.input cssClass=cssClass label=escape(label) name=namespacedFieldName type="text" value=fieldValue
	id="${namespacedFieldName}_hiddenUserList" style="display:none;">
		<#if required>
			<@liferay_aui.validator name="required" />
		</#if>
	</@liferay_aui.input>

<div id="userContainer">
	<input type="text" id="${namespacedFieldName}_userInput" class="form-control field form-control" name="user_input" value="" title="Users" />
	<ul id="${namespacedFieldName}_userList" class="helper-clearfix textboxlistentry-holder unstyled"></ul>
</div>

<script type="text/javascript">
	YUI().use('autocomplete', 'autocomplete-highlighters', 'datasource-io', 'datasource-jsonschema', 'highlight',
			function (Y) {

				function userFormatter(query, results) {
					return Y.Array.map(results, function (result) {
						var emailOption = '${emailOption}';
						var jobTitleOption = '${jobTitleOption}';
						var organizationsOption = '${organizationsOption}';
						var resultStr = result.raw.fullname;
						if (emailOption === "everywhere" || emailOption === "in_search") {
							resultStr += ' ' + result.raw.email
						}
						if ((jobTitleOption === "everywhere" || jobTitleOption === "in_search") && result.raw.jobTitle) {
							resultStr += ' (' + result.raw.jobTitle + ')'
						}
						if ((organizationsOption === "everywhere" || organizationsOption === "in_search") && result.raw
								.organizations) {
							resultStr += ' [' + result.raw.organizations + ']'
						}
						return Y.Highlight.all(resultStr, query);
					});
				}

				var defaultUsers =  [
                <#list values as user>
                    "${user}"<#if user_has_next>,</#if>
				</#list>
				];

				var selected = [];
				var clear = false;

				init();

				var dataSource = new Y.DataSource.IO({
					source: '/c/portal/userddm/users'
				});
				dataSource.plug(Y.Plugin.DataSourceJSONSchema, {schema: {}});



				Y.one('#${namespacedFieldName}_userInput').plug(Y.Plugin.AutoComplete, {
					source: dataSource,
					requestTemplate: '?inputVal={query}',
					resultTextLocator: function (result) {
						return result.email;
					},
					resultFormatter :  userFormatter,
					on: {
						select: function(event) {
							var item = event.result.raw;
							if (isUniqueInList(item.displayname)) {
                            <#if !multiple>
                                selected = [];
                                Y.one('[id="${namespacedFieldName}_userList"]').get('childNodes').remove();
							</#if>
								debugger;
								selected.push(item);

								updateHiddenUserList();

								addUserItem(item);

								addRemoveUserItemEvent(item);
							}
							clear = true;
						},
						activeItemChange: function(e) {
							if (clear) {
								clear = false;
								Y.one('#${namespacedFieldName}_userInput').set("value","");
							}

						}
					}
				});

				function init() {
					for(var i = 0; i < defaultUsers.length; i++) {
						var item = defaultUsers[i];
						var user = {displayname:item,fullname:item};

						for(var j = 0; j < users.length; j++) {
							if (users[j].displayname == item) {
								user = users[j];
								break;
							}
						}
						selected.push(user);

						addUserItem(user);

						addRemoveUserItemEvent(user);
					}
					updateHiddenUserList();
				}


				function isUniqueInList(name) {
					var names = [];
					for(var i = 0; i < selected.length; i++) {
						names.push(selected[i].displayname);
					}
					return (names.indexOf(name) == -1);
				}

				function updateHiddenUserList() {
					var values = [];
					for(var i = 0; i < selected.length; i++) {
						values.push(selected[i].displayname);
					}
					console.log(values);
					Y.one('[id$="${namespacedFieldName}_hiddenUserList"]').set("value", values);
				}

				function addUserItem(item) {
					<#assign showEmail = (emailOption == "everywhere" || emailOption == "in_result")>
					<#assign showJobTitle = (jobTitleOption == "everywhere" || jobTitleOption == "in_result") >
					<#assign showOrganizations = (organizationsOption == "everywhere" || organizationsOption ==	"in_result")>

					var userHtml = '<li class="yui3-widget component textboxlistentry" ' +
							'id="${namespacedFieldName}_userItem_'+item.displayname+'">' +

							'<a href="${usercardUrl}'+ item.email +'" class="textboxlistentry-text">' +
								item.fullname + '</a>' +

							'<#if showEmail><span class="textboxlistentry-text">' + item.email
							+'</span></#if>';

					if (item.jobTitle) {
						userHtml += '<#if showJobTitle><span class="textboxlistentry-text"> (' + item
										.jobTitle
								+')</span></#if>';
					}
					if (item.organizations) {
						userHtml += '<#if showOrganizations><span class="textboxlistentry-text"> [' + item
										.organizations
								+']</span></#if>';
					}
					userHtml += '<span class="textboxlistentry-remove">' +
							'<i class="icon icon-remove"></i>' +
							'</span>' +
							'</li>';

					Y.one('[id="${namespacedFieldName}_userList"]').append(
							userHtml);
				}

				function addRemoveUserItemEvent(item) {
					Y.one('[id="${namespacedFieldName}_userItem_'+item.displayname+'"] .textboxlistentry-remove').on("click", function (e) {
						Y.one('[id="${namespacedFieldName}_userItem_'+item.displayname+'"]').remove();
						var index = selected.indexOf(item);
						if (index > -1) {
							selected.splice(index, 1);
						}
						updateHiddenUserList();
					});
				}
			}
	);
</script>
</@>